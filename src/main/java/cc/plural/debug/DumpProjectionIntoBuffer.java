/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.debug;

import cc.plural.ecs.renderer.OrthogonalProjection;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;

/**
 *
 * @author John
 */
public class DumpProjectionIntoBuffer {

    public static void main(String[] args) {
        FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
        float left = 0;
        float right = 1;
        float top = 1;
        float bottom = 0;
        float near = 0;
        float far = 1;
        OrthogonalProjection projection = new OrthogonalProjection(left, right, top, bottom, near, far);
        matrix44Buffer.clear();
        projection.get(matrix44Buffer);
        matrix44Buffer.flip();
        float[] output = new float[16];
        matrix44Buffer.get(output);
        System.out.println("To Be Projection Matrix: \n" + matrix2String(output));
        
        matrix44Buffer.clear();
        projection.get(matrix44Buffer);
        matrix44Buffer.flip();
        matrix44Buffer.get(output);
        System.out.println("To Be Projection Matrix: \n" + matrix2String(output));
    }

    public static String matrix2String(float[] matrix) {

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < 4; i++) {
            builder.append('[').append('\t');

            for (int j = 0; j < 4; j++) {
                builder.append(matrix[i * 4 + j]).append('\t');
            }
            builder.append(']').append('\n');

        }

        return builder.toString();

    }
}
