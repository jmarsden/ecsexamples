/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.debug;

import cc.plural.ecs.renderer.OrthogonalProjection;
import cc.plural.utils.MatrixPrinter;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;

/**
 *
 * @author jmarsden
 */
public class ProjectionTest {
    
    public static void main(String[] args) {
        
        FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);
        
        float left = 0;
        float right = 1;
        float top = 1;
        float bottom = 0;
        float near = 0;
        float far = 1;

        // Setup projection matrix
        Matrix4f projectionMatrix = new Matrix4f();
        projectionMatrix.m00 = 2f / (right - left);
        projectionMatrix.m11 = 2f / (top - bottom);
        projectionMatrix.m22 = -2f / (far - near);
        projectionMatrix.m33 = 1f;

        projectionMatrix.m30 = -((right + left) / (right - left));
        projectionMatrix.m31 = -((top + bottom) / (top - bottom));
        projectionMatrix.m32 = -((far + near) / (far - near));
        
        projectionMatrix.store(matrix44Buffer);
        matrix44Buffer.flip();

        float[] output = new float[16];
        matrix44Buffer.get(output);
        
        System.out.println(MatrixPrinter.matrix2String(MatrixPrinter.MAJOR.COLUMN, output));
        
        OrthogonalProjection testProjection = new OrthogonalProjection(left, right, top, bottom, near, far);
        testProjection.load(output);
        
        System.out.println(MatrixPrinter.matrix2String(MatrixPrinter.MAJOR.ROW, output));
        
    }
    
}
