/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.debug;

import static cc.plural.debug.TestShaderCompile120.getFileContents;
import cc.plural.ecs.provider.StringLWJGLShader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;

/**
 *
 * @author jmarsden
 */
public class TestShaderCompile130 {

    public static void main(String[] args) throws LWJGLException {

        Display.create();

        String vertexSource = getFileContents("/assets/shaders/vertexDefault130.glsl");
        String fragmentSource = getFileContents("/assets/shaders/fragmentDefault130.glsl");

        StringLWJGLShader shader = new StringLWJGLShader(vertexSource, fragmentSource);
        shader.init();
        shader.load();

        System.out.println("Vertex Compile Error:" + shader.getVertexCompileError());
        System.out.println("Fragment Compile Error:" + shader.getFragmentCompileError());

        System.out.println(shader.getState());

        Display.destroy();
    }

    public static String getFileContents(String path) {

        StringBuilder shaderSource = new StringBuilder();
        int shaderID = 0;

        try {
            URL file = ClassLoader.class.getResource(path);

            InputStreamReader streamReader = new InputStreamReader(file.openStream());
            BufferedReader reader = new BufferedReader(streamReader);
            String line;
            while ((line = reader.readLine()) != null) {
                shaderSource.append(line).append("\n");
            }
            reader.close();
        } catch (IOException e) {
            System.err.println("Could not read file.");
            e.printStackTrace();
            System.exit(-1);
        }

        return shaderSource.toString();
    }
}
