package cc.plural.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.ByteBuffer;
import static org.lwjgl.opengl.GL11.GL_NO_ERROR;
import static org.lwjgl.opengl.GL11.glGetError;
import static org.lwjgl.opengl.GL20.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.*;
import org.lwjgl.util.glu.GLU;

public class SplitVboVertexVariousElementArrayBufferGL30 {

    public static void main(String[] args) throws LWJGLException {
        SplitVboVertexVariousElementArrayBufferGL30 example = new SplitVboVertexVariousElementArrayBufferGL30();
        example.initGL();
        example.run();
    }

    public void initGL() throws LWJGLException {
        // width and height of window and view port
        int width = 640;
        int height = 480;

        // set up window and display
        Display.setDisplayMode(new DisplayMode(width, height));
        Display.setVSyncEnabled(true);
        Display.setTitle("Shader Example 4 Ways - All Element Based");

        PixelFormat pixelFormat = new PixelFormat();
        ContextAttribs contextAtrributes = new ContextAttribs(3, 0);
        contextAtrributes.withForwardCompatible(true);
        Display.create(pixelFormat, contextAtrributes);

        GL11.glViewport(0, 0, width, height);
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    }

    public void run() {
        ShaderProgram shader = new ShaderProgram();

        shader.init("/assets/shaders/vertexPosition130.glsl", "/assets/shaders/fragmentColor130.glsl");

        int vaoHandle = constructVertexArrayObject();

        byte[] indicesTopLeft = {
            1, 0, 3, 1, 3, 4
        };

        ByteBuffer indicesTopLeftBuffer = BufferUtils.createByteBuffer(indicesTopLeft.length);
        indicesTopLeftBuffer.put(indicesTopLeft);
        indicesTopLeftBuffer.flip();

        int vboiId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesTopLeftBuffer, GL15.GL_STATIC_DRAW);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);

        byte[] indicesTopRight = {
            2, 1, 4, 2, 4, 5
        };

        ByteBuffer indicesTopRightBuffer = BufferUtils.createByteBuffer(indicesTopRight.length);
        indicesTopRightBuffer.put(indicesTopRight);
        indicesTopRightBuffer.flip();

        int[] indicesBottomLeft = {
            4, 3, 6, 4, 6, 7
        };

        ByteBuffer indicesBottomLeftBuffer = BufferUtils.createByteBuffer(indicesBottomLeft.length * 4);
        IntBuffer indicesBottomLeftBufferInt = indicesBottomLeftBuffer.asIntBuffer();
        indicesBottomLeftBufferInt.put(indicesBottomLeft);
        indicesBottomLeftBufferInt.flip();

        while (Display.isCloseRequested() == false) {
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

            // tell OpenGL to use the shader
            GL20.glUseProgram(shader.getProgramId());

            // bind vertex and color data
            GL30.glBindVertexArray(vaoHandle);
            GL20.glEnableVertexAttribArray(0); // VertexPosition
            GL20.glEnableVertexAttribArray(1); // VertexColor

            GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);
            GL11.glDrawElements(GL11.GL_TRIANGLES, indicesTopLeft.length, GL11.GL_UNSIGNED_BYTE, 0);
            GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);

            GL11.glDrawElements(GL11.GL_TRIANGLES, indicesTopRightBuffer);

            GL11.glDrawElements(GL11.GL_TRIANGLES, indicesBottomLeftBufferInt);

            // check for errors
            if (glGetError() != GL_NO_ERROR) {
                throw new RuntimeException("OpenGL error: " + GLU.gluErrorString(glGetError()));
            }

            GL20.glDisableVertexAttribArray(1); // VertexColor
            GL20.glDisableVertexAttribArray(0); // VertexPosition

            // bind vertex and color data
            GL30.glBindVertexArray(0);
            // tell OpenGL to use the shader
            GL20.glUseProgram(0);

            GL11.glMatrixMode(GL11.GL_PROJECTION);
            GL11.glLoadIdentity();
            GL11.glOrtho(-1, 1, -1, 1, 1, -1);
            GL11.glMatrixMode(GL11.GL_MODELVIEW);

            GL11.glBegin(GL11.GL_QUADS);
            GL11.glColor3f(1f, 1f, 0f);
            GL11.glVertex2f(0, 0);
            GL11.glColor3f(0f, 0f, 0f);
            GL11.glVertex2f(0, -1);
            GL11.glColor3f(0.5f, 0.5f, 0.5f);
            GL11.glVertex2f(1, -1);
            GL11.glColor3f(1f, 0f, 1f);
            GL11.glVertex2f(1, 0);
            GL11.glEnd();

            // swap buffers and sync frame rate to 60 fps
            Display.update();
            Display.sync(60);
        }

        Display.destroy();
    }

    /**
     * Create Vertex Array Object necessary to pass data to the shader
     */
    private int constructVertexArrayObject() {
        // create vertex data 
        float[] positionData = new float[]{
            -1f, 1f, 0f,
            0f, 1f, 0f,
            1f, 1f, 0f,
            -1f, 0f, 0f,
            0f, 0f, 0f,
            1f, 0f, 0f,
            -1f, -1f, 0f,
            0f, -1f, 0f,
            1f, -1f, 0f
        };

        // create color data
        float[] colorData = new float[]{
            0f, 0f, 1f,
            1f, 0f, 0f,
            0f, 1f, 0f,
            1f, 1f, 1f,
            1f, 1f, 0f,
            1f, 0f, 1f,
            0f, 1f, 1f,
            0f, 0f, 0f,
            0.5f, 0.5f, 0.5f,};

        // convert vertex array to buffer
        FloatBuffer positionBuffer = BufferUtils.createFloatBuffer(positionData.length);
        positionBuffer.put(positionData);
        positionBuffer.flip();

        // convert color array to buffer
        FloatBuffer colorBuffer = BufferUtils.createFloatBuffer(colorData.length);
        colorBuffer.put(colorData);
        colorBuffer.flip();

        // create vertex byffer object (VBO) for vertices
        int positionBufferHandle = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, positionBufferHandle);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, positionBuffer, GL15.GL_STATIC_DRAW);

        // create VBO for color values
        int colorBufferHandle = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, colorBufferHandle);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, colorBuffer, GL15.GL_STATIC_DRAW);

        // unbind VBO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        // create vertex array object (VAO)
        int vaoHandle = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoHandle);
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);

        // assign vertex VBO to slot 0 of VAO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, positionBufferHandle);
        GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 0, 0);

        // assign vertex VBO to slot 1 of VAO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, colorBufferHandle);
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0);

        // unbind VBO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        return vaoHandle;
    }

    public class ShaderProgram {
        // OpenGL handle that will point to the executable shader program
        // that can later be used for rendering

        private int programId;

        public void init(String vertexShaderFilename, String fragmentShaderFilename) {
            // create the shader program. If OK, create vertex and fragment shaders
            programId = glCreateProgram();

            // load and compile the two shaders
            int vertShader = loadAndCompileShader(vertexShaderFilename, GL_VERTEX_SHADER);
            int fragShader = loadAndCompileShader(fragmentShaderFilename, GL_FRAGMENT_SHADER);

            // attach the compiled shaders to the program
            glAttachShader(programId, vertShader);
            glAttachShader(programId, fragShader);

            // now link the program
            glLinkProgram(programId);

            // validate linking
            if (glGetProgram(programId, GL_LINK_STATUS) == GL11.GL_FALSE) {
                throw new RuntimeException("could not link shader. Reason: " + glGetProgramInfoLog(programId, 1000));
            }

            // perform general validation that the program is usable
            glValidateProgram(programId);

            if (glGetProgram(programId, GL_VALIDATE_STATUS) == GL11.GL_FALSE) {
                throw new RuntimeException("could not validate shader. Reason: " + glGetProgramInfoLog(programId, 1000));
            }
        }

        /*
         * With the exception of syntax, setting up vertex and fragment shaders
         * is the same.
         * @param the name and path to the vertex shader
         */
        private int loadAndCompileShader(String filename, int shaderType) {
            //vertShader will be non zero if succefully created
            int handle = glCreateShader(shaderType);

            if (handle == 0) {
                throw new RuntimeException("could not created shader of type " + shaderType + " for file " + filename + ". " + glGetProgramInfoLog(programId, 1000));
            }

            // load code from file into String
            String code = loadFile(filename);

            // upload code to OpenGL and associate code with shader
            glShaderSource(handle, code);

            // compile source code into binary
            glCompileShader(handle);

            // acquire compilation status
            int shaderStatus = glGetShader(handle, GL20.GL_COMPILE_STATUS);

            // check whether compilation was successful
            if (shaderStatus == GL11.GL_FALSE) {
                throw new IllegalStateException("compilation error for shader [" + filename + "]. Reason: " + glGetShaderInfoLog(handle, 1000));
            }

            return handle;
        }

        /**
         * Load a text file and return it as a String.
         */
        private String loadFile(String filename) {
            StringBuilder shaderSource = new StringBuilder();
            try {
                URL file = ClassLoader.class.getResource(filename);

                InputStreamReader streamReader = new InputStreamReader(file.openStream());
                BufferedReader reader = new BufferedReader(streamReader);
                String line;
                while ((line = reader.readLine()) != null) {
                    shaderSource.append(line).append("\n");
                }
                reader.close();
            } catch (IOException e) {
                System.err.println("Could not read file.");
                e.printStackTrace();
                System.exit(-1);
            }
            return shaderSource.toString();
        }

        public int getProgramId() {
            return programId;
        }
    }
}