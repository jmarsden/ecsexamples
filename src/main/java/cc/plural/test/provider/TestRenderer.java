package cc.plural.test.provider;

import cc.plural.ecs.engine.GameObject;
import cc.plural.ecs.provider.FileLWJGLTexture;
import cc.plural.ecs.provider.LWJGLDisplay;
import cc.plural.ecs.provider.LWJGLInMemoryMeshVAO;
import cc.plural.ecs.provider.LWJGLRenderer;
import cc.plural.ecs.provider.StringLWJGLShader;
import cc.plural.ecs.renderer.GLSLVersion;
import static cc.plural.ecs.renderer.GLSLVersion.GLSL430;
import cc.plural.ecs.renderer.Mesh;
import cc.plural.ecs.renderer.Shader;
import cc.plural.ecs.renderer.ShaderDefaults;
import cc.plural.ecs.renderer.Texture;
import cc.plural.graphics.Vertex;
import org.lwjgl.opengl.GL11;
import cc.plural.math.Matrix4f;
import cc.plural.math.Transformation;
import cc.plural.utils.ArrayPrinter;
import cc.plural.utils.MatrixPrinter;
import cc.plural.utils.PNGDecoder;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector3f;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author John
 */
public class TestRenderer extends LWJGLRenderer {


    boolean firstRenderCycle;

    /**
     * Debug
     */
    /**
     * @deprecated
     */
    private int vaoId = 0;
    /**
     * @deprecated
     */
    private int vboId = 0;
    /**
     * @deprecated
     */
    private int vboiId = 0;
    /**
     * @deprecated
     */
    private int indicesCount = 0;
    /**
     * @deprecated
     */
    private Vertex[] vertices = null;
    /**
     * @deprecated
     */
    private ByteBuffer verticesByteBuffer = null;
    // Shader variables
    /**
     * @deprecated
     */
    private int vsId = 0;
    /**
     * @deprecated
     */
    private int fsId = 0;
    /**
     * @deprecated
     */
    private int pId = 0;
    /**
     * @deprecated
     */
    private Shader testShader;
    // Texture variables
    /**
     * @deprecated
     */
    private int[] texIds = new int[]{0, 0};
    /**
     * @deprecated
     */
    private int textureSelector = 1;

    // LWJGL Objects
    /**
     * @deprecated
     */
    private Vector3f modelPos = null;
    /**
     * @deprecated
     */
    private Vector3f modelAngle = null;
    /**
     * @deprecated
     */
    private Vector3f modelScale = null;
    /**
     * @deprecated
     */
    private Vector3f cameraPos = null;

    /**
     * @deprecated
     */
    private cc.plural.math.Transformation ecsTransform = null;
    /**
     * @deprecated
     */
    private FloatBuffer matrix44Buffer = null;
    /**
     * @deprecated
     */
    private LWJGLInMemoryMeshVAO testMesh = null;
    /**
     * @deprecated
     */
    private Texture[] textures = null;

    public TestRenderer(LWJGLDisplay display) {
        super(display);
        firstRenderCycle = true;
    }

    @Override
    public void init() {
        GL11.glClearColor(backgroundColor.getRed() / 255F, backgroundColor.getGreen() / 255F, backgroundColor.getBlue() / 255F, 1.0f);

        switch (getGLSLVersion()) {
            case GLSL110:
                throw new RuntimeException("Dont Support " + GLSLVersion.GLSL110);
            case GLSL120:
                throw new RuntimeException("Dont Support " + GLSLVersion.GLSL120);
            case GLSL130:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_130, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_130);
                }
                break;
            case GLSL140:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_140, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_140);
                }
                break;
            case GLSL150:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_150, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_150);
                }
                break;
            case GLSL330:
            case GLSL400:
            case GLSL410:
            case GLSL420:
            case GLSL430:
                if (defaultShader == null) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_150, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_150);
                }
                break;

            default:
                if (getGLSLVersion().getVersion() > GLSL430.getVersion()) {
                    createDefaultShader(ShaderDefaults.DEFAULT_VERTEX_SHADER_150, ShaderDefaults.DEFAULT_FRAGMENT_SHADER_150);
                }

                throw new RuntimeException("Dont Support " + getGLSLVersion());
        }

        if (!defaultShader.isInitialized()) {
            defaultShader.init();
        }
        if (!defaultShader.isLoaded()) {
            defaultShader.load();
        }
        if (defaultShader.inError()) {
            throw new RuntimeException("Default Shader No Compile: " + defaultShader.getVertexCompileError() + " " + defaultShader.getFragmentCompileError() + " " + defaultShader.getLinkError());
        }

//        ecsProjectionMatrix = new Matrix4f();
//        ecsViewMatrix = Matrix4f.identity();
//        ecsModelMatrix = Matrix4f.identity();

        /* **
         * Debug
         */
        //setupShaders();
        //shader = getDefaultShader();
        //setupTextures();
        //setupQuad();
    }

    public StringLWJGLShader createShader(String vertexSource, String fragmentSource) {
        return new StringLWJGLShader(vertexSource, fragmentSource);
    }

    public StringLWJGLShader createShader(URL vertexURL, URL fragmentURL) {
        return new StringLWJGLShader(ClassLoader.class.getResource("/assets/shaders/vertexDefault130.glsl"),
                ClassLoader.class.getResource("/assets/shaders/fragmentDefault130.glsl"));
    }

    public void releaseShader(Shader shader) {
        shader.release();
    }

    public StringLWJGLShader createDefaultShader(String vertexSource, String fragmentSource) {
        if (defaultShader != null) {
            defaultShader.release();
        }
        defaultShader = createShader(vertexSource, fragmentSource);
        return defaultShader;
    }

    public StringLWJGLShader createDefaultShader(URL vertexURL, URL fragmentURL) {
        if (defaultShader != null) {
            defaultShader.release();
        }
        defaultShader = createShader(vertexURL, fragmentURL);
        return defaultShader;
    }

    public StringLWJGLShader getDefaultShader() {
        return defaultShader;
    }

    public void completeRender() {

        if (!defaultShader.isLoaded()) {
            defaultShader.load();
        }

        if (skipRender) {
            batch.clear();
            lock = false;
            return;
        }

        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

        projection.load(projectionMatrix);

        camera.load(viewMatrix);

        for (GameObject gameObject : batch.queue) {

            if (gameObject.geometry == null || !gameObject.geometry.isValid()) {
                continue;
            }
            if (gameObject.spatial == null) {
                continue;
            }

            Shader shader = getDefaultShader();
            Texture texture = gameObject.geometry.texture;
            Mesh mesh = gameObject.geometry.mesh;

            Transformation transformation = gameObject.getWorld();
            transformation.load(modelMatrix);

            texture.enable();
            {
                shader.enable();
                {

                    if (firstRenderCycle) {
                        System.out.println("First Render Cycle Debug");
                        System.out.println("Shader:" + shader.getState());
                        System.out.println("Texture:" + texture.toString());
                        System.out.println("Mesh:" + mesh.dumpState());
                        System.out.println("ECS Othogonal Projection Matrix: \n" + MatrixPrinter.matrix2String(projectionMatrix));
                        System.out.println("ECS View Matrix: \n" + MatrixPrinter.matrix2String(viewMatrix));
                        System.out.println("ECS Model Matrix: \n" + MatrixPrinter.matrix2String(modelMatrix));
                        firstRenderCycle = false;
                    }

                    shader.setUniform(shader.getUniformLocation("projectionMatrix"), projectionMatrix);
                    shader.setUniform(shader.getUniformLocation("viewMatrix"), viewMatrix);
                    shader.setUniform(shader.getUniformLocation("modelMatrix"), modelMatrix);

                    mesh.enable(shader);
                    GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.getIndicesCount(), GL11.GL_UNSIGNED_SHORT, 0);
                    mesh.disable(shader);
                }
                shader.disable();
            }
            texture.disable();
        }

        batch.clear();
        lock = false;
    }

    private void setupQuad() {

        /**
         * LWJGL Setup Quad
         */
        // We'll define our quad using 4 vertices of the custom 'TexturedVertex' class
        Vertex v0 = new Vertex();
        v0.setXYZ(0f, 1f, 0);
        v0.setRGB(0, 0, 1);
        v0.setST(0, 0);
        Vertex v1 = new Vertex();
        v1.setXYZ(0f, 0f, 0);
        v1.setRGB(0, 1, 1);
        v1.setST(0, 1);
        Vertex v2 = new Vertex();
        v2.setXYZ(1f, 0f, 0);
        v2.setRGB(1, 0, 1);
        v2.setST(1, 1);
        Vertex v3 = new Vertex();
        v3.setXYZ(1f, 1f, 0);
        v3.setRGB(1, 1, 0);
        v3.setST(1, 0);

        vertices = new Vertex[]{v0, v1, v2, v3};

        // Put each 'Vertex' in one FloatBuffer
        verticesByteBuffer = BufferUtils.createByteBuffer(vertices.length * Vertex.STRIDE);
        FloatBuffer verticesFloatBuffer = verticesByteBuffer.asFloatBuffer();
        for (int i = 0; i < vertices.length; i++) {
            // Add position, color and texture floats to the buffer
            verticesFloatBuffer.put(vertices[i].getElements());
        }
        verticesFloatBuffer.flip();

        System.out.println("LWJGL Vertex Buffer:\t" + ArrayPrinter.arrayToString(verticesFloatBuffer));
        verticesFloatBuffer.rewind();

        // OpenGL expects to draw vertices in counter clockwise order by default
        short[] indices = {
            0, 1, 2,
            2, 3, 0
        };

        indicesCount = indices.length;
        ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(indicesCount * 2);
        ShortBuffer indiciesShortBuffer = indicesBuffer.asShortBuffer();
        indiciesShortBuffer.put(indices);
        indiciesShortBuffer.flip();
        System.out.println("LWJGL Indices Buffer:\t" + ArrayPrinter.arrayToString(indiciesShortBuffer));
        indiciesShortBuffer.rewind();

        // Create a new Vertex Array Object in memory and select it (bind)
        vaoId = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoId);

        // Create a new Vertex Buffer Object in memory and select it (bind)
        vboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesFloatBuffer, GL15.GL_STREAM_DRAW);

        // Put the position coordinates in attribute list 0
        GL20.glVertexAttribPointer(getDefaultShader().getAttributeLocation("in_Position"), Vertex.POSITION_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.POSITION_BYTE_OFFSET);
        // Put the color components in attribute list 1
        GL20.glVertexAttribPointer(getDefaultShader().getAttributeLocation("in_Color"), Vertex.COLOR_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.COLOR_BYTE_OFFSET);
        // Put the texture coordinates in attribute list 2
        GL20.glVertexAttribPointer(getDefaultShader().getAttributeLocation("in_TextureCoord"), Vertex.TEXTURE_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.TEXTURE_BYTE_OFFSET);

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        // Deselect (bind to 0) the VAO
        GL30.glBindVertexArray(0);

        // Create a new VBO for the indices and select it (bind) - INDICES
        vboiId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indiciesShortBuffer, GL15.GL_STATIC_DRAW);

        //  int result = GL15.glGetBufferParameter(vboiId, GL15.GL_BUFFER_SIZE);
        //System.out.println("Result: " + result);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);

        // Set the default quad rotation, scale and position values
        modelPos = new Vector3f(0, 0, 0);
        modelAngle = new Vector3f(0, 0, 0);
        modelScale = new Vector3f(1, 1, 1);
        cameraPos = new Vector3f(0, 0, 0);

        /**
         * ECS Setup Quad
         */
        testMesh = new LWJGLInMemoryMeshVAO(vertices, indices);
        testMesh.init();
        testMesh.load();

        System.out.println(testMesh.dumpState());
    }

    /**
     * @deprecated
     */
    private void setupTextures() {
        texIds[0] = this.loadPNGTexture("/assets/images/grid1.png", GL13.GL_TEXTURE0);
        texIds[1] = this.loadPNGTexture("/assets/images/texture1.png", GL13.GL_TEXTURE0);

        textures = new Texture[2];
        textures[0] = new FileLWJGLTexture("/assets/images/grid1.png");
        textures[0].init();
        textures[0].load();
        textures[1] = new FileLWJGLTexture("/assets/images/texture1.png");
        textures[1].init();
        textures[1].load();
    }

    private int loadPNGTexture(String filename, int textureUnit) {
        ByteBuffer buf = null;
        int tWidth = 0;
        int tHeight = 0;

        try {
            // Open the PNG file as an InputStream
            URL file = ClassLoader.class.getResource(filename);

            // Link the PNG decoder to this stream
            PNGDecoder decoder = new PNGDecoder(file.openStream());

            // Get the width and height of the texture
            tWidth = decoder.getWidth();
            tHeight = decoder.getHeight();

            // Decode the PNG file in a ByteBuffer
            buf = ByteBuffer.allocateDirect(
                    4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(buf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            buf.flip();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        // Create a new texture object in memory and bind it
        int texId = GL11.glGenTextures();
        GL13.glActiveTexture(textureUnit);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);

        // All RGB bytes are aligned to each other and each component is 1 byte
        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

        // Upload the texture data and generate mip maps (for scaling)
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, tWidth, tHeight, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buf);
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);

        // Setup the ST coordinate system
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

        // Setup what to do when the texture has to be scaled
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);

        return texId;
    }
}
