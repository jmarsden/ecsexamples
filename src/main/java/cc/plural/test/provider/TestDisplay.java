/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.test.provider;

import cc.plural.ecs.provider.LWJGLDisplay;
import cc.plural.ecs.provider.LWJGLRendererGL15;
import cc.plural.ecs.provider.LWJGLRendererGL2;
import cc.plural.ecs.renderer.GLVersion;
import cc.plural.ecs.renderer.Renderer;
import cc.plural.ecs.runtime.ApplicationConfiguration;

/**
 *
 * @author John
 */
public class TestDisplay extends LWJGLDisplay {

    @Override
    public TestRenderer createRenderer(ApplicationConfiguration configuration) {
        if (!displayCreated) {
            throw new RuntimeException("Must init");
        }
        return new TestRenderer(this);
    }
    
}