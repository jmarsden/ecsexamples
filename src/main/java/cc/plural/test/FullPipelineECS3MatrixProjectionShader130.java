/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.test;

import cc.plural.ecs.component.spatial.SpatialComponent;
import cc.plural.ecs.provider.FileLWJGLTexture;
import cc.plural.ecs.provider.LWJGLInMemoryMeshVAO;
import cc.plural.ecs.provider.StringLWJGLShader;
import cc.plural.ecs.renderer.Camera;
import cc.plural.ecs.renderer.OrthogonalProjection;
import cc.plural.ecs.renderer.Shader;
import cc.plural.ecs.renderer.SimpleCamera;
import cc.plural.ecs.renderer.Texture;
import cc.plural.graphics.Vertex;
import cc.plural.utils.ArrayPrinter;
import cc.plural.utils.MatrixPrinter;
import cc.plural.utils.PNGDecoder;
import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author John
 */
public class FullPipelineECS3MatrixProjectionShader130 {
    // Entry point for the application

    public static void main(String[] args) {
        (new FullPipelineECS3MatrixProjectionShader130()).start();
    }
    // Setup variables
    private final String WINDOW_TITLE = "ECS FullPipelineECS3MatrixProjectionShader130";
    private final int WIDTH = 600;
    private final int HEIGHT = 600;
    private final double PI = 3.14159265358979323846;
    // Quad variables
    private int vaoId = 0;
    private int vboId = 0;
    private int vboiId = 0;
    private int indicesCount = 0;
    private Vertex[] vertices = null;
    private ByteBuffer verticesByteBuffer = null;
    // Shader variables
    private int vsId = 0;
    private int fsId = 0;
    private int pId = 0;
    private Shader shader;
    // Texture variables
    private int[] texIds = new int[]{0, 0};
    private int textureSelector = 1;
    // Moving variables
    private int projectionMatrixLocation = 0;
    private int viewMatrixLocation = 0;
    private int modelMatrixLocation = 0;
    // LWJGL Objects
    private Matrix4f projectionMatrix = null;
    private Matrix4f viewMatrix = null;
    private Matrix4f modelMatrix = null;
    private Vector3f modelPos = null;
    private Vector3f modelAngle = null;
    private Vector3f modelScale = null;
    private Vector3f cameraPos = null;
    // ECS Objects
    private cc.plural.math.Matrix4f ecsProjectionMatrix = null;
    private cc.plural.math.Matrix4f ecsViewMatrix = null;
    private cc.plural.math.Matrix4f ecsModelMatrix = null;
    private cc.plural.math.Transformation ecsTransform = null;
    private FloatBuffer matrix44Buffer = null;
    private LWJGLInMemoryMeshVAO mesh = null;
    private Texture[] textures = null;

    public FullPipelineECS3MatrixProjectionShader130() {
    }

    private void start() {
        setupOpenGL();

        setupShaders();
        setupTextures();
        setupQuad();
        setupMatrices();

        while (!Display.isCloseRequested()) {
            this.loopCycle();
            Display.sync(60);
            Display.update();
        }

        destroyOpenGL();
    }

    private void setupOpenGL() {
        try {
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.setTitle(WINDOW_TITLE);
            Display.create();

            //GL11.glViewport(0, 0, WIDTH, HEIGHT);
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        // Setup an XNA like background color
        GL11.glClearColor(0.8f, 0.8f, 0.8f, 0f);

        // Map the internal OpenGL coordinate system to the entire screen
        GL11.glViewport(0, 0, WIDTH, HEIGHT);

        this.exitOnGLError("setupOpenGL");
    }

    private void setupShaders() {
        shader = new StringLWJGLShader(
                ClassLoader.class.getResource("/assets/shaders/vertexDefault130.glsl"),
                ClassLoader.class.getResource("/assets/shaders/fragmentDefault130.glsl"));
        shader.init();
        shader.load();

        projectionMatrixLocation = shader.getUniformLocation("projectionMatrix");
        viewMatrixLocation = shader.getUniformLocation("viewMatrix");
        modelMatrixLocation = shader.getUniformLocation("modelMatrix");
    }

    private void setupTextures() {
        texIds[0] = this.loadPNGTexture("/assets/images/grid1.png", GL13.GL_TEXTURE0);
        texIds[1] = this.loadPNGTexture("/assets/images/texture1.png", GL13.GL_TEXTURE0);

        textures = new Texture[2];
        textures[0] = new FileLWJGLTexture("/assets/images/grid1.png");
        textures[0].init();
        textures[0].load();
        textures[1] = new FileLWJGLTexture("/assets/images/texture1.png");
        textures[1].init();
        textures[1].load();

        this.exitOnGLError("setupTexture");
    }

    private void setupQuad() {

        /**
         * LWJGL Setup Quad
         */
        Vertex v0 = new Vertex();
        v0.setXYZ(0f, 1f, 0);
        v0.setRGB(1, 0, 0);
        v0.setST(0, 0);
        v0.setN(0, 0, 1);
        Vertex v1 = new Vertex();
        v1.setXYZ(0f, 0f, 0);
        v1.setRGB(0, 1, 0);
        v1.setST(0, 1);
        v1.setN(0, 0, 1);
        Vertex v2 = new Vertex();
        v2.setXYZ(1f, 0f, 0);
        v2.setRGB(0, 0, 1);
        v2.setST(1, 1);
        v2.setN(0, 0, 1);
        Vertex v3 = new Vertex();
        v3.setXYZ(1f, 1f, 0);
        v3.setRGB(1, 1, 1);
        v3.setST(1, 0);
        v3.setN(0, 0, 1);
        vertices = new Vertex[]{v0, v1, v2, v3};

        // Put each 'Vertex' in one FloatBuffer
        verticesByteBuffer = BufferUtils.createByteBuffer(vertices.length * Vertex.STRIDE);
        FloatBuffer verticesFloatBuffer = verticesByteBuffer.asFloatBuffer();
        for (int i = 0; i < vertices.length; i++) {
            // Add position, color and texture floats to the buffer
            verticesFloatBuffer.put(vertices[i].getElements());
        }
        verticesFloatBuffer.flip();

        System.out.println("LWJGL Vertex Buffer:\t" + ArrayPrinter.arrayToString(verticesFloatBuffer));
        verticesFloatBuffer.rewind();

        // OpenGL expects to draw vertices in counter clockwise order by default
        short[] indices = {
            0, 1, 2,
            2, 3, 0
        };

        indicesCount = indices.length;
        ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(indicesCount * 2);
        ShortBuffer indiciesShortBuffer = indicesBuffer.asShortBuffer();
        indiciesShortBuffer.put(indices);
        indiciesShortBuffer.flip();
        System.out.println("LWJGL Indices Buffer:\t" + ArrayPrinter.arrayToString(indiciesShortBuffer));
        indiciesShortBuffer.rewind();

        // Create a new Vertex Array Object in memory and select it (bind)
        vaoId = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoId);

        // Create a new Vertex Buffer Object in memory and select it (bind)
        vboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesFloatBuffer, GL15.GL_STREAM_DRAW);

        // Put the position coordinates in attribute list 0
        //GL20.glVertexAttribPointer(shader.getAttributeLocation("in_Position"), Vertex.POSITION_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.POSITION_BYTE_OFFSET);
        // Put the color components in attribute list 1
        //GL20.glVertexAttribPointer(shader.getAttributeLocation("in_Color"), Vertex.COLOR_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.COLOR_BYTE_OFFSET);
        // Put the texture coordinates in attribute list 2
        //GL20.glVertexAttribPointer(shader.getAttributeLocation("in_TextureCoord"), Vertex.TEXTURE_ELEMENT_COUNT, GL11.GL_FLOAT, false, Vertex.STRIDE, Vertex.TEXTURE_BYTE_OFFSET);

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        // Deselect (bind to 0) the VAO
        GL30.glBindVertexArray(0);

        // Create a new VBO for the indices and select it (bind) - INDICES
        vboiId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indiciesShortBuffer, GL15.GL_STATIC_DRAW);

        //  int result = GL15.glGetBufferParameter(vboiId, GL15.GL_BUFFER_SIZE);
        //System.out.println("Result: " + result);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);

        // Set the default quad rotation, scale and position values
        modelPos = new Vector3f(0, 0, 0);
        modelAngle = new Vector3f(0, 0, 0);
        modelScale = new Vector3f(1, 1, 1);
        cameraPos = new Vector3f(0, 0, 0);

        /**
         * ECS Setup Quad
         */
        mesh = new LWJGLInMemoryMeshVAO(vertices, indices);
        mesh.init();
        mesh.load();

        System.out.println(mesh.dumpState());

        this.exitOnGLError("setupQuad");
    }

    private void setupMatrices() {
        // Create a FloatBuffer with the proper size to store our matrices later
        matrix44Buffer = BufferUtils.createFloatBuffer(16);

        setupProjection();
        setupView();
        setupModel();
    }

    private void setupProjection() {
        float[] output = new float[16];

        // Setup projection matrix
        float left = 0;
        float right = 1;
        float top = 1;
        float bottom = 0;
        float near = 0;
        float far = 1;

        /**
         * LWJGL Setup Projection Matrix
         */
        projectionMatrix = new Matrix4f();
        projectionMatrix.m00 = 2f / (right - left);
        projectionMatrix.m11 = 2f / (top - bottom);
        projectionMatrix.m22 = -2f / (far - near);
        projectionMatrix.m33 = 1f;

        projectionMatrix.m30 = -((right + left) / (right - left));
        projectionMatrix.m31 = -((top + bottom) / (top - bottom));
        projectionMatrix.m32 = -((far + near) / (far - near));

        projectionMatrix.store(matrix44Buffer);
        matrix44Buffer.flip();
        matrix44Buffer.get(output);
        System.out.println("DEBUG Othogonal Projection Matrix: \n" + MatrixPrinter.glMatrix2String(output));

        /**
         * ECS Setup Projection Matrix
         */
        OrthogonalProjection projection = new OrthogonalProjection(left, right, top, bottom, near, far);
        ecsProjectionMatrix = new cc.plural.math.Matrix4f();
        projection.load(ecsProjectionMatrix);
        System.out.println("ECS Othogonal Projection Matrix: \n" + MatrixPrinter.matrix2String(ecsProjectionMatrix));

    }

    private void setupView() {
        float[] output = new float[16];
        /**
         * LWJGL Setup View Matrix
         */
        viewMatrix = new Matrix4f();

        matrix44Buffer.clear();
        viewMatrix.store(matrix44Buffer);
        matrix44Buffer.flip();

        matrix44Buffer.get(output);
        System.out.println("View Matrix: \n" + MatrixPrinter.glMatrix2String(output));
        matrix44Buffer.flip();

        /**
         * ECS Setup Projection Matrix
         */
        Camera camera = new SimpleCamera();
        ecsViewMatrix = new cc.plural.math.Matrix4f();
        camera.load(ecsViewMatrix);
        System.out.println("ECS View Matrix: \n" + MatrixPrinter.matrix2String(ecsViewMatrix));
    }

    private void setupModel() {
        float[] output = new float[16];
        /**
         * LWJGL Setup Model Matrix
         */
        modelMatrix = new Matrix4f();
        modelMatrix.m30 = 0f;
        modelMatrix.m31 = 0f;

        matrix44Buffer.clear();
        modelMatrix.store(matrix44Buffer);
        matrix44Buffer.flip();

        matrix44Buffer.get(output);
        System.out.println("Model Matrix: \n" + MatrixPrinter.glMatrix2String(output));
        matrix44Buffer.flip();

        SpatialComponent spatial = new SpatialComponent();
        ecsModelMatrix = new cc.plural.math.Matrix4f();
        spatial.load(ecsModelMatrix);
        System.out.println("ECS Model Matrix: \n" + MatrixPrinter.matrix2String(ecsViewMatrix));
    }

    private void logicCycle() {
        //-- Input processing
        float rotationDelta = 15f;
        float scaleDelta = 0.01f;
        float posDelta = 0.01f;
        Vector3f scaleAddResolution = new Vector3f(scaleDelta, scaleDelta, scaleDelta);
        Vector3f scaleMinusResolution = new Vector3f(-scaleDelta, -scaleDelta, -scaleDelta);

        while (Keyboard.next()) {
            // Only listen to events where the key was pressed (down event)
            if (!Keyboard.getEventKeyState()) {
                continue;
            }

            // Switch textures depending on the key released
            switch (Keyboard.getEventKey()) {
                case Keyboard.KEY_1:
                    textureSelector = 0;
                    break;
                case Keyboard.KEY_2:
                    textureSelector = 1;
                    break;
            }

            // Change model scale, rotation and translation values
            switch (Keyboard.getEventKey()) {
                // Move
                case Keyboard.KEY_UP:
                    modelPos.y += posDelta;
                    break;
                case Keyboard.KEY_DOWN:
                    modelPos.y -= posDelta;
                    break;
                // Scale
                case Keyboard.KEY_P:
                    Vector3f.add(modelScale, scaleAddResolution, modelScale);
                    break;
                case Keyboard.KEY_M:
                    Vector3f.add(modelScale, scaleMinusResolution, modelScale);
                    break;
                // Rotation
                case Keyboard.KEY_LEFT:
                    modelPos.x -= posDelta;
                    break;
                case Keyboard.KEY_RIGHT:
                    modelPos.x += posDelta;
                    break;
            }
        }

        modelAngle.z += 0.1;

        //-- Update matrices
        // Reset view and model matrices
        viewMatrix = new Matrix4f();

        ecsModelMatrix.setData(0, 3, modelPos.x);
        ecsModelMatrix.setData(1, 3, modelPos.y);

        // Upload matrices to the uniform variables
        shader.enable();
        shader.setUniform(projectionMatrixLocation, ecsProjectionMatrix);
        shader.setUniform(viewMatrixLocation, ecsViewMatrix);
        shader.setUniform(modelMatrixLocation, ecsModelMatrix);
        shader.disable();

        this.exitOnGLError("logicCycle");
    }

    private void renderCycle() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
        textures[textureSelector].enable();

        shader.enable();

        mesh.enable();
        GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.indicesCount, GL11.GL_UNSIGNED_SHORT, 0);
        mesh.disable();

        shader.disable();

        textures[textureSelector].disable();
        this.exitOnGLError("renderCycle");
    }

    private void loopCycle() {
        // Update logic
        this.logicCycle();
        // Update rendered frame
        this.renderCycle();

        this.exitOnGLError("loopCycle");
    }

    private void destroyOpenGL() {
        // Delete the texture
        GL11.glDeleteTextures(texIds[0]);
        GL11.glDeleteTextures(texIds[1]);

        // Delete the shaders
        GL20.glUseProgram(0);
        GL20.glDetachShader(pId, vsId);
        GL20.glDetachShader(pId, fsId);

        GL20.glDeleteShader(vsId);
        GL20.glDeleteShader(fsId);
        GL20.glDeleteProgram(pId);

        shader.disable();
        shader.unLoad();
        shader.release();

        // Select the VAO
        GL30.glBindVertexArray(vaoId);

        // Disable the VBO index from the VAO attributes list
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);

        // Delete the vertex VBO
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL15.glDeleteBuffers(vboId);

        // Delete the index VBO
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        GL15.glDeleteBuffers(vboiId);

        // Delete the VAO
        GL30.glBindVertexArray(0);
        GL30.glDeleteVertexArrays(vaoId);

        Display.destroy();
    }

    private int loadPNGTexture(String filename, int textureUnit) {
        ByteBuffer buf = null;
        int tWidth = 0;
        int tHeight = 0;

        try {
            // Open the PNG file as an InputStream
            URL file = ClassLoader.class.getResource(filename);

            // Link the PNG decoder to this stream
            PNGDecoder decoder = new PNGDecoder(file.openStream());

            // Get the width and height of the texture
            tWidth = decoder.getWidth();
            tHeight = decoder.getHeight();

            // Decode the PNG file in a ByteBuffer
            buf = ByteBuffer.allocateDirect(
                    4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(buf, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            buf.flip();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        // Create a new texture object in memory and bind it
        int texId = GL11.glGenTextures();
        GL13.glActiveTexture(textureUnit);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texId);

        // All RGB bytes are aligned to each other and each component is 1 byte
        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

        // Upload the texture data and generate mip maps (for scaling)
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, tWidth, tHeight, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buf);
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);

        // Setup the ST coordinate system
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);

        // Setup what to do when the texture has to be scaled
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);

        this.exitOnGLError("loadPNGTexture");

        return texId;
    }

    private void exitOnGLError(String errorMessage) {
        int errorValue = GL11.glGetError();

        if (errorValue != GL11.GL_NO_ERROR) {
            String errorString = GLU.gluErrorString(errorValue);
            System.err.println("ERROR - " + errorMessage + ": " + errorString);

            if (Display.isCreated()) {
                Display.destroy();
            }
            System.exit(-1);
        }
    }
}
