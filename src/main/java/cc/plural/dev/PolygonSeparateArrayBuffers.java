/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.dev;

import cc.plural.graphics.Vertex;
import cc.plural.utils.ArrayPrinter;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

public class PolygonSeparateArrayBuffers {

    public static final String TITLE = "Title";
    public static final int WIDTH = 500;
    public static final int HEIGHT = 500;
    int vboId;
    int vboiId;

    public void init() {
        try {
            Display.setTitle(TITLE);
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.create();
        } catch (Exception e) {
            System.err.println(e.toString());

            System.exit(1);
        }

        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glEnable(GL11.GL_DEPTH_TEST);


        Vertex vertex1 = new Vertex();
        Vertex vertex2 = new Vertex();
        Vertex vertex3 = new Vertex();
        vertex1.setXYZ(100, 100, 0f);
        vertex1.setRGBA(1, 0, 0, 1);
        vertex2.setXYZ(100 + 100, 100, 100f);
        vertex2.setRGBA(1, 0, 0, 1);
        vertex3.setXYZ(100 + 200, 100 + 200, 100f);
        vertex3.setRGBA(1, 0, 0, 1);

        float[] vertex = vertex1.getElements();

        ArrayPrinter.printArray(vertex);

        System.out.println("Count:" + Vertex.ELEMENT_COUNT);
        System.out.println("Bytes:" + Vertex.ELEMENT_FLOAT_BYTES);
        System.out.println("Stride:" + Vertex.STRIDE + " " + (Vertex.ELEMENT_COUNT * Vertex.ELEMENT_FLOAT_BYTES) + " " + (Vertex.POSITION_BYTE_COUNT+Vertex.NORMAL_BYTE_COUNT+Vertex.COLOR_BYTE_COUNT));

        System.out.println("Position Byte Offset:" + Vertex.POSITION_BYTE_OFFSET);
        System.out.println("Position Byte Count:" + Vertex.POSITION_BYTE_COUNT);

        System.out.println("Normal Byte Offset:" + Vertex.NORMAL_BYTE_OFFSET);
        System.out.println("Normal Byte Count:" + Vertex.NORMAL_BYTE_COUNT);

        System.out.println("Color Byte Offset:" + Vertex.COLOR_BYTE_OFFSET);
        System.out.println("Color Byte Count:" + Vertex.COLOR_BYTE_COUNT);

        System.out.println("ST Byte Offset:" + Vertex.TEXTURE_BYTE_OFFSET);
        System.out.println("ST Byte Count:" + Vertex.TEXTURE_BYTE_COUNT);

        Vertex[] vertices = new Vertex[]{vertex1, vertex2, vertex3};
        ByteBuffer verticesByteBuffer = null;
        // Put each 'Vertex' in one FloatBuffer
        verticesByteBuffer = BufferUtils.createByteBuffer(vertices.length * Vertex.STRIDE);
        FloatBuffer verticesFloatBuffer = verticesByteBuffer.asFloatBuffer();
        for (int i = 0; i < vertices.length; i++) {
            // Add position, color and texture floats to the buffer
            verticesFloatBuffer.put(vertices[i].getElements());
        }
        verticesFloatBuffer.rewind();

        float[] temp = new float[verticesFloatBuffer.capacity()];
        verticesFloatBuffer.get(temp);

        ArrayPrinter.printArray(temp);
        ArrayPrinter.printArray(vertex);

        vboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesFloatBuffer, GL15.GL_STATIC_DRAW);

        short[] elements = new short[]{0, 1, 2};
        ByteBuffer elementByteBuffer = null;
        elementByteBuffer = BufferUtils.createByteBuffer(elements.length*4);
        ShortBuffer elementShortBuffer = elementByteBuffer.asShortBuffer();

        elementShortBuffer.put(elements);
        elementShortBuffer.rewind();

        vboiId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, elementShortBuffer, GL15.GL_STATIC_DRAW);

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, 800, 0, 600, 1, -1);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
    }

    public void start() {

        while (!Display.isCloseRequested()) {
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

            GL11.glPushMatrix();

            //GL20.glEnableVertexAttribArray(0);
            //GL20.glEnableVertexAttribArray(1);
            //GL20.glEnableVertexAttribArray(2);

            GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
            GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
            GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);

            //GL20.glEnableVertexAttribArray(vboId);

            GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId);
            GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);

            GL11.glVertexPointer(4, GL11.GL_FLOAT, Vertex.POSITION_BYTE_COUNT+Vertex.NORMAL_BYTE_COUNT+Vertex.COLOR_BYTE_COUNT, Vertex.POSITION_BYTE_OFFSET);
            GL11.glNormalPointer(GL11.GL_FLOAT, Vertex.POSITION_BYTE_COUNT+Vertex.NORMAL_BYTE_COUNT+Vertex.COLOR_BYTE_COUNT, Vertex.NORMAL_BYTE_OFFSET);
            GL11.glColorPointer(4, GL11.GL_FLOAT, Vertex.POSITION_BYTE_COUNT+Vertex.NORMAL_BYTE_COUNT+Vertex.COLOR_BYTE_COUNT, Vertex.COLOR_BYTE_OFFSET);

            
            GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, 3);
            //GL11.glDrawElements(GL11.GL_TRIANGLES, 3, GL11.GL_UNSIGNED_BYTE, 0);

            //GL20.glDisableVertexAttribArray(0);

            GL11.glDisableClientState(GL11.GL_VERTEX_ARRAY);
            GL11.glDisableClientState(GL11.GL_NORMAL_ARRAY);
            GL11.glDisableClientState(GL11.GL_COLOR_ARRAY);

            GL11.glPopMatrix();

// set the color of the quad (R,G,B,A)
            GL11.glColor3f(0.5f, 0.5f, 1.0f);

// draw quad
            GL11.glBegin(GL11.GL_QUADS);
            GL11.glVertex2f(100, 100);
            GL11.glVertex2f(100 + 200, 100);
            GL11.glVertex2f(100 + 200, 100 + 200);
            GL11.glVertex2f(100, 100 + 200);
            GL11.glEnd();

            Display.sync(60);
            Display.update();
        }


    }

    public void exit() {
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        GL15.glDeleteBuffers(vboId);

        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        GL15.glDeleteBuffers(vboiId);

        Display.destroy();
    }

    public static void main(String[] args) {

        PolygonSeparateArrayBuffers application = new PolygonSeparateArrayBuffers();
        application.init();
        application.start();
        application.exit();
    }
}
